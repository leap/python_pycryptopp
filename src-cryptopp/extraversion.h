
/* This _version.py is generated from git metadata by the pycryptopp
 * setup.py. The main version number is taken from the most recent release
 * tag. If some patches have been added since the last release, this will
 * have a -NN "build number" suffix, or else a -rNN "revision number" suffix.
 */

#define CRYPTOPP_EXTRA_VERSION "pycryptopp-0.6.0.1206569328141510525648634803928199668821045408958"
